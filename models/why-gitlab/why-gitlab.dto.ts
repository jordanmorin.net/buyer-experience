import { PageDTO } from '../page.dto';

export interface WhyGitlabDTO extends PageDTO {
  hero?: any;
  banner?: any;
  categories_block?: any;
  badges?: any;
  pricing?: any;
}
