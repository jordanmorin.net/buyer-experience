## Goal
We would like X which will do  Z.

#### Please provide more information related to this engineering request
Make sure the information you provide is relevant for your request. If unsure, please provide all the fields. Add/remove rows as needed.

## Which page(s) are involved in this request?
* `[Page Title](URL)`

## Include any Figma files
* `[Figma](URL)`

## Include any graphic or design assets
* `Upload any extra files needed to complete this request`

## In scope
What is within scope of this request?

- [ ] Deliverable 1
- [ ] Deliverable 2


## DCI
[DRI, Consulted, Informed](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#dri-consulted-informed-dci)

- [ ] DRI: 'Engineer'
- [ ] Consulted: `@justin.vetter @laurenbarker @fqureshi`
- [ ] Informed: `Everyone`

/label ~"dex-status::triage"  ~"dex::engineering"
