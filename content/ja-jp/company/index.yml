---
  title: GitLabについて
  description: GitLab社について、そしてGitLabの魅力についてご紹介します。
  components:
    - name: 'solutions-hero'
      data:
        title: GitLabについて
        subtitle: GitLabの舞台裏で描かれるDevSecOpsプラットフォームの未来
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/company/company_hero.png
          image_url_mobile: /nuxt-images/company/company_hero.png
          alt: "GitLabチームメンバー"
          bordered: true
          rectangular: true
    - name: 'copy-about'
      data:
        title: "当社の事業"
        subtitle: "当社は、最も包括的なDevSecOpsプラットフォームであるGitLabを提供している会社です。"
        description: |
          2011年に、1つのプログラマーチームにおける共同作業を支援するオープンソースプロジェクトとして、GitLabは誕生しました。セキュリティとコンプライアンスを強化しながら、より迅速かつ効率的なソフトウェアデリバリーを実現するプラットフォームとして、今では何百万人もの人々に使用されるようになりました。
          \
          設立当初から、リモートワーク、オープンソース、DevSecOps、イテレーションへの確固たる信念を持ち続けてきました。当社チームは起床後（もしくは任意の勤務開始時間に）ログオンし、皆様がツールチェーンに時間を取られずに、優れたコードをより速くリリースすることに集中できるよう、GitLabコミュニティと協力して毎月新しいイノベーションを届けています。
        cta_text: 'GitLabの詳細について'
        cta_link: '/why-gitlab/'
        data_ga_name: "about gitlab"
        data_ga_location: "body"
    - name: 'copy-numbers'
      data:
        title: 数字で知るGitLab
        rows:
          - item:
            - col: 4
              title: |
                **コード**
                **コントリビューター**
              number: "3,300人以上"
              link: http://contributors.gitlab.com/
              data_ga_name: contributors
              data_ga_location: body
            - col: 4
              title: |
                **オフィス数**
                （創業当初から完全リモート）
              number: "0"
          - item:
            - col: 4
              title: |
                **連続リリース数**
                （毎月22日）
              number: "133回"
              link: /releases/
              data_ga_name: releases
              data_ga_location: body
            - col: 4
              title: |
                **チームメンバー**
                （60か国以上）
              number: "1,800人以上"
              link: /company/team/
              data_ga_name: team
              data_ga_location: body
          - item:
            - col: 8
              title: '**推定登録ユーザー数**'
              number: "3,000万人以上"
        customer_logos_block:
          showcased_enterprises:
            - image_url: "/nuxt-images/home/logo_tmobile_white.svg"
              link_label: T-MobileとGitLabのウェブキャストのランディングページへのリンク
              alt: "T-Mobileロゴ"
              url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
            - image_url: "/nuxt-images/home/logo_goldman_sachs_white.svg"
              link_label: Goldman Sachsの顧客事例へのリンク
              alt: "Goldman Sachsロゴ"
              url: "/customers/goldman-sachs/"
            - image_url: "/nuxt-images/home/logo_cncf_white.svg"
              link_label: Cloud Native Computing Foundationの顧客事例へのリンク
              alt: "Cloud Nativeロゴ"
              url: /customers/cncf/
            - image_url: "/nuxt-images/home/logo_siemens_white.svg"
              link_label: Siemensの顧客事例へのリンク
              alt: "Siemensロゴ"
              url: /customers/siemens/
            - image_url: "/nuxt-images/home/logo_nvidia_white.svg"
              link_label: NVIDIAの顧客事例へのリンク
              alt: "NVIDIAロゴ"
              url: /customers/nvidia/
            - image_url: "/nuxt-images/home/logo_ubs_white.svg"
              link_label: ブログ投稿『How UBS created their own DevOps platform using GitLab』へのリンク
              alt: "UBSのロゴ"
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        cta_title: |
          計画から本番稼働に至るまで、
          単一のアプリケーションでチーム間の足並みを揃えよう
        cta_text: "無料トライアルを開始"
        cta_link: "https://gitlab.com/-/trial_registrations/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
        data_ga_name: "free trial"
        data_ga_location: "body"
    - name: 'copy-mission'
      data:
        title: "GitLabのミッション"
        description: "私たちのミッションは、**誰もがコントリビュートできる**ようにすることです。誰もがコントリビュートできるようになれば、ユーザーがコントリビューターとなり、イノベーションを大幅に加速できます。"
        cta_text: '詳細はこちら'
        cta_link: '/company/mission/'
        data_ga_name: "mission"
        data_ga_location: "body"
    - name: 'showcase'
      data:
        title: GitLabのバリュー
        items:
          - title: 共同作業
            icon:
              name: collaboration-alt-4
              alt: コラボレーションアイコン
              variant: marketing
            text: "私たちは、前向きな意図があると想定すること、「ありがとう」や「ごめんなさい」を言うこと、そしてタイムリーにフィードバックをすることなど、効果的に共同作業を行う上で役立つことを大切にしています。"
            link:
              href: /handbook/values/#collaboration
              data_ga_name: Collaboration
              data_ga_location: body
          - title: 結果
            icon:
              name: increase
              alt: 増加アイコン
              variant: marketing
            text: "私たちは、行動志向で緊迫感を持って、お互いに、お客様に、ユーザーに、そして投資家に約束したことを実行します。"
            link:
              href: /handbook/values/#results
              data_ga_name: results
              data_ga_location: body
          - title: 効率
            icon:
              name: digital-transformation
              alt: 効率アイコン
              variant: marketing
            text: "退屈なソリューションの選択からすべてを文書化し、自己管理することまで、私たちは正しいことを迅速に進めるよう努めています。"
            link:
              href: /handbook/values/#efficiency
              data_ga_name: efficiency
              data_ga_location: body
          - title: "多様性、インクルージョン、つながり"
            icon:
              name: community
              alt: コミュニティアイコン
              variant: marketing
            text: "私たちは、GitLabがさまざまなバックグラウンドや状況にある人たちの居場所になり、その人たちが成長できる場となるように取り組んでいます。"
            link:
              href: "/handbook/values/#diversity-inclusion"
              data_ga_name: diversity inclusion
              data_ga_location: body
          - title: イテレーション
            icon:
              name: continuous-delivery
              alt: 継続的デリバリーアイコン
              variant: marketing
            text: "私たちは、実行可能で価値のある最小限のことを行い、迅速にフィードバックを得ることを目指しています。"
            link:
              href: "/handbook/values/#iteration"
              data_ga_name: iteration
              data_ga_location: body
          - title: 透明性
            icon:
              name: open-book
              alt: 書籍アイコン
              variant: marketing
            text: "自社のハンドブックから製品のイシュートラッカーまで、私たちは行っていることをすべてデフォルトで公開しています。"
            link:
              href: "/handbook/values/#transparency"
              data_ga_name: transparency
              data_ga_location: body
    - name: 'timeline'
      data:
        title: "GitLabの歩み"
        items:
          - year: "2011"
            description: "GitLabプロジェクトは1つのコミットからはじまりました"
          - year: ""
            description: "毎月22日にGitLabの新バージョンをリリースするようになりました"
          - year: "2012"
            description: "GitLab CIの最初のバージョンが作成されました"
          - year: "2014"
            description: "GitLabが法人化されました"
          - year: "2015"
            description: "Y Combinatorに参加し、GitLabハンドブックをウェブサイトのリポジトリに公開しました"
          - year: "2016"
            description: "マスタープランを発表し、シリーズBの資金調達で2,000万ドルを調達しました"
          - year: "2021"
            description: "GitLab Inc.がNASDAQ Global Market（NASDAQ：GTLB）の上場企業となりました"
    - name: 'copy-about'
      data:
        title: "GitLabで働く"
        description: |
          私たちは、世界中のすべてのチームメンバーが自分らしさを発揮し、最善を尽くし、自分の声が届き、歓迎されていると感じ、ワークライフバランスを真に優先できるような、完全なリモート環境を作り上げることを目指しています。
          \
          チームの一員になることにご興味がある場合は、[GitLabでの働き方の詳細](/jobs/)をご覧いただき、適していると思われる募集職種にぜひご応募ください。
        cta_text: '募集中の職種をすべて表示する'
        cta_link: '/jobs/all-jobs/'
        data_ga_name: "all jobs"
        data_ga_location: "body"
    - name: 'teamops'
      data:
        image: "/nuxt-images/company/company-teamops.svg"
        image_alt: "ドキュメントを共有する同僚の姿"
        mobile_img: "/nuxt-images/company/TeamOps-mobile.svg"
        header_img: "/nuxt-images/company/TeamOps.svg"
        title: "最高のチームで迅速な進化を。そしてより良い未来を。"
        description: "TeamOpsは、チームワークを客観的な専門分野としてとらえる、GitLab独自の人事プラクティスです。これこそが、GitLabがスタートアップから10年で世界的な上場企業に成長できた秘訣です。無料で入手可能なプラクティショナー認定により、他の組織もTeamOpsを活用して、より良い意思決定を行い、結果を出し、世界を前進させることができます。"
        button:
            link: "/teamops/"
            text: "TeamOpsの詳細について"
            data_ga_name: "learn more about teamops"
            data_ga_location: "body"
        link:
            link: "https://levelup.gitlab.com/learn/course/teamops"
            text: "認定を受ける"
            data_ga_name: "get certified"
            data_ga_location: "body"
    - name: 'learn-more-cards'
      data:
        title: 関連リンク
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: ブログのアイコン
              hex_color: "#171321"
            event_type: "ブログ"
            header: "GitLab、DevOps、セキュリティなどの最新情報をご覧ください。"
            link_text: "ブログを見る"
            href: "/blog/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLabロゴ
            data_ga_name: "blog"
            data_ga_location: "body"
          - icon:
              name: announcement
              variant: marketing
              alt: お知らせアイコン
              hex_color: "#171321"
            event_type: "プレスルーム"
            header: "最近のニュース、プレスリリース、プレスキットをご覧ください。"
            link_text: "詳細はこちら"
            href: "/press/press-kit/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLabロゴ
            data_ga_name: "press kit"
            data_ga_location: "body"
          - icon:
              name: money
              variant: marketing
              alt: お金のアイコン
              hex_color: "#171321"
            event_type: "投資家向け情報"
            header: "投資家向けの最新の株式および財務情報はこちらをご覧ください。"
            link_text: "詳細はこちら"
            href: "https://ir.gitlab.com/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLabロゴ
            data_ga_name: "investor relations"
            data_ga_location: "body"

