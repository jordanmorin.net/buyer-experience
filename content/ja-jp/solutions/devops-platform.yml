---
  title: GitLab - DevOpsプラットフォーム
  description: 単一のユーザーインターフェース、連結型データ保管、およびDevOpsライフサイクル内の埋め込み式セキュリティを備えた単一アプリケーションとして提供される完全版DevOpsプラットフォームです。
  components:
    - name: 'solutions-hero'
      data:
        title: DevOpsプラットフォーム
        subtitle: GitLabは、DevSecOpsプラットフォームのすべての機能を備えた単一アプリケーションであり、組織がセキュリティとコンプライアンスを強化しながら、ソフトウェアをより迅速に提供することを可能にします。これにより、ソフトウェア開発の利益率が最大限に高まります。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
        secondary_btn:
          text: デモを視聴
          url: /demo/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像：DevOpsプラットフォームのGitLab"
    - name: 'copy-media'
      data:
        block:
          - header: DevOpsプラットフォーム
            id: the-dev-ops-platform
            text: |
              DevOpsツールは問題を解決するべきものであり、問題の原因となるべきではありません。DevOpsの連携促進が進むにつれ、ポイントソリューションで構成された脆弱なツールチェーンは破損して、コスト増加や可視性の低下を招き、価値ではなく摩擦をもたらします。ユーザー構築によるツールチェーンとは対照的に、真のDevOpsプラットフォームは、チームのイテレーションを速め、チームが協力しながらイノベーションを実現できるようにします。このプラットフォームの使命は、リスクとコストを抑えながら、より高品質で安全なソフトウェアをより迅速に提供するために必要な機能をすべて提供するために、複雑性とリスクを排除することです。
            link_href: /demo/
            link_text: 詳しくはこちら
            video:
              video_url: https://www.youtube.com/embed/-_CDU6NFw7U?enablesjsapi=1
    - name: 'benefits'
      data:
        cards_per_row: 3
        benefits:
          - title: 作業効率を向上
            description: |
              ブロッカーを特定し、単一のツール上で即座に対処できます。
            icon:
              name: gitlab-release
              alt: GitLabリリースのアイコン
              variant: marketing
              hex_color: '#451D7E'
          - title: より高品質なソフトウェアをより迅速に提供
            description: |
              インテグレーションの維持ではなく、価値の提供に焦点を当てます。
            icon:
              name: agile
              alt: アジャイルのアイコン
              variant: marketing
              hex_color: '#451D7E'
          - title: リスクとコストを削減
            description: |
              スピードや予算を維持しながら、セキュリティとコンプライアンスの確保を自動化できます。
            icon:
              name: lock-cog
              alt: 南京錠と歯車のアイコン
              variant: marketing
              hex_color: '#451D7E'
    - name: 'copy-media'
      data:
        block:
          - header: GitLab DevOpsプラットフォームのメリット
            id: git-lab-dev-ops-platform-benefits
            text: |
              DevOpsライフサイクル全体に対応した単一アプリケーションであるGitLabは、次のような特徴を備えています。
              * **包括的** 作業を行うシステム内でのプラットフォーム全体の分析を通じて、DevOpsライフサイクル全体を可視化および最適化します。
              * **シームレス** ワークフローにおいて混乱を引き起こす可能性のあるサードパーティのプラグインやAPIに依存することなく、異なるチームおよび各ライフサイクルステージ全体で共通のツールセットを使用できます。
              * **安全** 各コミットにおける脆弱性とコンプライアンス違反をスキャンできます。
              * **透明性とコンプライアンス** 監査やレトロスペクティブの実施において追跡を容易にするために、計画からコード変更、承認に至るまで、あらゆるアクションを自動的に取得して関連付けます。
              * **導入の容易性** 単一のUXを理解し、単一のデータストアを管理するだけで済みます。また、実行はすべて任意のインフラストラクチャで完結します。
            image:
              image_url: /nuxt-images/topics/devops-lifecycle.svg
              alt: ""
    - name: 'pull-quote'
      data:
        quote: 当社は、エンジニアが実際に使いたいと考えるプラットフォームを社内に導入することで、複数のチームへの導入を促進し、誰かに「強制」することなく生産性を向上させることができます。これは、「より豊富なリリース、より優れた管理、より高品質なソフトウェア」という当社の戦略目標に向けて前進するために、エンドユーザーによる積極的なサポートを受けられるエコシステムを構築する上で大いに役立っています。
        source: George Grant
        link_href: /customers/goldman-sachs/
        link_text: 詳しくはこちら
        data_ga_name: george grant quote
        data_ga_location: body
        shadow: true
    - name: 'copy-media'
      data:
        block:
          - header: リソース
            id: resources
            text: |
              * **[Gartner:](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html){data-ga-name="gartner" data-ga-location="body"}** DevOpsバリューストリーム配信プラットフォームのマーケットガイド
              * **[Glympse:](/customers/glympse/){data-ga-name="glympse" data-ga-location="body"}** GitLabに20個のツールを統合
              * **[BI WORLDWIDE](/customers/bi_worldwide/){data-ga-name="BI worldwide" data-ga-location="body"}** 単一プラットフォームを活用してコードを保護
            video:
              video_url: https://www.youtube.com/embed/gzYTZhJlHoI?enablesjsapi=1

