### Setting up your development environment


## Setting up your project for the first time

1. Set up ssh key pair with your GitLab account. [Configure your SSH client to point to the directory where the](https://docs.gitlab.com/ee/user/ssh.html#configure-ssh-to-point-to-a-different-directory) private key is stored
1. Install Xcode
1. install visual studio code
1. Install adsf package manager - OR use the [online install script to set up the GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation) This will set up your development enviornment nicely for you. 
1. `yarn install` 
1. `git clone git@gitlab.com:gitlab-com/marketing/digital-experience/buyer-experience.git`
1. `git checkout -b some-epic-branch`
1. Made a change, `git add .` , `git -m "my commit message"`
1. `git push origin some-epic-branch`
1. Navigate to the Buyer Experience repository in GitLab, a banner should pop up asking if you'd like to create a merge request from your branch. Do that. In you MR, check the comit history to see if the right profile is attached. If it's not, go back to step 1 an tweak your ssh key pair setup.




